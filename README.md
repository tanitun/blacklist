# BLACK LIST #

Implementation of black list of clients

## Description ##

Simple CRUD web application for managing black list of clients with authorization

## Build and run ##

### Preconditions ###

Git, Java, Maven

### Steps ###

* Clone the project
* mvn clean package
* java -jar target/blacklist-0.0.1-SNAPSHOT.jar
* The application will be available on http://localhost:8080/clients. Login - user, password - password
