package com.kholod.blacklist.controller;

import com.kholod.blacklist.service.ClientService;
import com.kholod.blacklist.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ClientController {

    private ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/")
    public String root() {
        return "redirect:/clients";
    }

    @GetMapping("/clients")
    public String getClients(Model model) {
        model.addAttribute("clients", clientService.getClients());
        return "clients";
    }

    @GetMapping("/clients/add")
    public String addClient(Client client) {
        return "update-client";
    }

    @PostMapping("/clients")
    public String updateClient(Client client, Model model) {
        clientService.saveClient(client);
        model.addAttribute("clients", clientService.getClients());
        return "redirect:/clients";
    }

    @GetMapping("/clients/edit/{id}")
    public String editClient(@PathVariable("id") int id, Model model) {
        Client client = clientService.getClientById(id);
        model.addAttribute("client", client);
        return "update-client";
    }

    @GetMapping("/clients/delete/{id}")
    public String deleteClient(@PathVariable("id") int id, Model model) {
        clientService.deleteClient(id);
        model.addAttribute("clients", clientService.getClients());
        return "redirect:/clients";
    }

}
