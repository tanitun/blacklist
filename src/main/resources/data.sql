INSERT INTO client (name, phone, email)
SELECT * FROM (
SELECT 'John Connor', '+380670000001', 'jconnor@skynet.org' union
SELECT 'Sarah Connor', '+380670000002', 'sconnor@skynet.org' union
SELECT 'T-800', '+380670000003', 't800@skynet.org'
)
WHERE NOT EXISTS (SELECT * FROM client);